//
//  BCYPointsCreater.m
//  BCYefanovTest
//
//  Created by Sergey Yefanov on 22.03.16.
//  Copyright © 2016 Sergey Yefanov. All rights reserved.
//

#import "BCYPointsCreater.h"

@implementation BCYPointsCreater

+ (NSArray *)getPoints {
    return @[
             @{@"title" : @"Moscow 1",
               @"longitude" : @(55.70),
               @"latitude" : @(37.60)},
             
             @{@"title" : @"Moscow 2",
               @"longitude" : @(55.80),
               @"latitude" : @(37.70)},
             
             @{@"title" : @"Moscow 3",
               @"longitude" : @(55.60),
               @"latitude" : @(37.50)},
             
             
             ];
}

@end
