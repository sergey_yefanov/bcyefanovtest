//
//  MDDirectionService.h
//  googleMaps
//
//  Created by Eric Vennaro on 3/23/14.
//  Copyright (c) 2014 Eric Vennaro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDDirectionService : NSObject
/**
 *  Get directions
 */
- (void)directionsWithQuery:(NSDictionary *)query completion:(void (^)(NSDictionary * dict))completion;
/**
 *  Get times
 */
- (void)timeWithQuery:(NSDictionary *)query completion:(void (^)(NSDictionary * dict))completion;

@end
