//
//  ViewController.m
//  BCYefanovTest
//
//  Created by Sergey Yefanov on 22.03.16.
//  Copyright © 2016 Sergey Yefanov. All rights reserved.
//

#import "BCYMainVC.h"
#import "MDDirectionService.h"
#import "BCYPointsCreater.h"
@import GoogleMaps;

NS_ENUM(NSInteger, BCYAlertViewButton) {
    BCYAlertViewButtonDriving,
    BCYAlertViewButtonWalking,
    BCYAlertViewButtonBicycling,
    BCYAlertViewButtonTransit
};

@interface BCYMainVC () <GMSMapViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>
/**
 *  Google map view
 */
@property (nonatomic, strong) GMSMapView *mapView;
/**
 *  Time label
 */
@property (nonatomic, strong) UILabel *timeLabel;
/**
 *  Current user's location
 */
@property (nonatomic, assign) CLLocationCoordinate2D currentLocation;
/**
 *  End point
 */
@property (nonatomic, assign) CLLocationCoordinate2D endPoint;
/**
 *  Polyline for route
 */
@property (strong, nonatomic) GMSPolyline *polyline;
/**
 *  Location manager
 */
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation BCYMainVC

#pragma mark - UIViewController methods

- (void)viewDidLoad {
    [super viewDidLoad];

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:55.70
                                                            longitude:37.60
                                                                 zoom:10];
    self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.indoorEnabled = YES;
    self.mapView.accessibilityElementsHidden = NO;
    self.mapView.settings.scrollGestures = YES;
    self.mapView.settings.zoomGestures = YES;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    self.view = self.mapView;
    
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.timeLabel];

    [self addMarkers];
    [self setupLocationManager];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.timeLabel.frame = CGRectMake(0, 20, self.view.bounds.size.width, 40);
}

#pragma mark - Setup

- (void)setupLocationManager {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
}

- (void)updateTimeWithDictionary:(NSDictionary *)dict {
    NSString *timeText = [[dict[@"rows"] firstObject][@"elements"] firstObject][@"duration"][@"text"];
    
    if (!timeText.length) {
        return;
    }
    
    self.timeLabel.backgroundColor = [UIColor whiteColor];
    self.timeLabel.text = [NSString stringWithFormat:@"Время в пути: %@", timeText];
}

#pragma mark - Private

- (void)addMarkers {
    NSArray *markers = [BCYPointsCreater getPoints];
    
    for (NSDictionary *marker in markers) {
        [self addMarker:marker];
    }
}

- (GMSMarker *)addMarker:(NSDictionary *)markerDict {
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([markerDict[@"longitude"] doubleValue],
                                                 [markerDict[@"latitude"] doubleValue]);
    marker.title = markerDict[@"title"];
    marker.map = self.mapView;
    return marker;
}

- (void)addDirections:(NSDictionary *)json {
    NSDictionary *routes = json[@"routes"][0];
    NSDictionary *route = routes[@"overview_polyline"];
    NSString *overview_route = route[@"points"];
    GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
    self.polyline = [GMSPolyline polylineWithPath:path];
    self.polyline.map = self.mapView;
}

- (void)reset {
    self.polyline.map = nil;
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.text = @"";
}

#pragma mark - CLLocation manager delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations {
    self.currentLocation = [[manager location] coordinate];
    [self.mapView animateToLocation:self.currentLocation];
}

#pragma mark - MKMapView delegate methods

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(nonnull GMSMarker *)marker {
    [self reset];
    self.endPoint = CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude);

    [[[UIAlertView alloc] initWithTitle:@"Выберите тип передвижения"
                                message:nil delegate:self
                      cancelButtonTitle:nil
                      otherButtonTitles:@"На автомобиле", @"Пешком", @"На велосипеде", @"На общественном транспорте", @"Отмена", nil] show];

    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    [self reset];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSMutableArray *coordinatesStrings = [NSMutableArray new];
    [coordinatesStrings addObject:[NSString stringWithFormat:@"%f,%f", self.currentLocation.latitude, self.currentLocation.longitude]];
    [coordinatesStrings addObject:[NSString stringWithFormat:@"%f,%f", self.endPoint.latitude, self.endPoint.longitude]];
    
    MDDirectionService *directiorService = [[MDDirectionService alloc] init];
    
    NSString *mode = nil;
    
    switch (buttonIndex) {
        case BCYAlertViewButtonWalking:
            mode = @"walking";
            break;
            
        case BCYAlertViewButtonDriving:
            mode = @"driving";
            break;
            
        case BCYAlertViewButtonBicycling:
            mode = @"bicycling";
            break;
            
        case BCYAlertViewButtonTransit:
            mode = @"transit";
            break;
            
        default:
            return;
            break;
    }
    
    NSDictionary *query = @{@"mode" : mode, @"waypoints" : coordinatesStrings};
    
    [directiorService directionsWithQuery:query completion:^(NSDictionary *dict) {
        if (![dict[@"routes"] count]) {
            [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Невозможно построить маршрут" delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil, nil] show];
            [self reset];
            return;
        }
        
        [self addDirections:dict];
    }];
    
    [directiorService timeWithQuery:query completion:^(NSDictionary *dict) {
        [self updateTimeWithDictionary:dict];
    }];

}

@end
