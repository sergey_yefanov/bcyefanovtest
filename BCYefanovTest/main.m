//
//  main.m
//  BCYefanovTest
//
//  Created by Sergey Yefanov on 22.03.16.
//  Copyright © 2016 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
