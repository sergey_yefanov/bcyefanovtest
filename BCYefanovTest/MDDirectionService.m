//
//  MDDirectionService.m
//  googleMaps
//
//  Created by Eric Vennaro on 3/23/14.
//  This code is based directly from provided documentation in the google api documents
//  And tutorial section.  Can find sources on the google maps API direction services
//  Copyright (c) 2014 Eric Vennaro. All rights reserved.
//

#import "MDDirectionService.h"

static NSString * const kMDDirectionsURL = @"https://maps.googleapis.com/maps/api/directions/json?&origin=%@&destination=%@&mode=%@&key=AIzaSyB667H2LKDeitNJhtDfXh9-tBkcEryzUNQ";
static NSString * const kMDDistanceMatrixURL = @"https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=%@&destinations=%@&mode=%@&language=ru&key=AIzaSyB667H2LKDeitNJhtDfXh9-tBkcEryzUNQ";

@implementation MDDirectionService

#pragma mark - Public

- (void)directionsWithQuery:(NSDictionary *)query completion:(void (^)(NSDictionary * dict))completion {
    NSArray *waypoints = query[@"waypoints"];
    NSMutableString *url = [NSMutableString stringWithFormat:kMDDirectionsURL, waypoints.firstObject, waypoints.lastObject, query[@"mode"]];
    
    url = [[url stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding] mutableCopy];
    NSURL *directionsURL = [NSURL URLWithString:url];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSData* data = [NSData dataWithContentsOfURL:directionsURL];
        [self fetchedData:data withCompletion:completion];
    });
}

- (void)timeWithQuery:(NSDictionary *)query completion:(void (^)(NSDictionary *))completion {
    NSArray *waypoints = query[@"waypoints"];
    NSMutableString *url = [NSMutableString stringWithFormat:kMDDistanceMatrixURL, waypoints.firstObject, waypoints.lastObject, query[@"mode"]];
    
    url = [[url stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding] mutableCopy];
    NSURL *directionsURL = [NSURL URLWithString:url];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSData *data = [NSData dataWithContentsOfURL:directionsURL];
        [self fetchedData:data withCompletion:completion];
    });
}

#pragma - Private

- (void)fetchedData:(NSData *)data withCompletion:(void (^)(NSDictionary * dict))completion {
    NSError *error;
    NSDictionary *json = [NSJSONSerialization
                          JSONObjectWithData:data
                          options:kNilOptions
                          error:&error];
    completion(json);
}

@end
