//
//  AppDelegate.h
//  BCYefanovTest
//
//  Created by Sergey Yefanov on 22.03.16.
//  Copyright © 2016 Sergey Yefanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

