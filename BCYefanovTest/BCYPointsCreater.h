//
//  BCYPointsCreater.h
//  BCYefanovTest
//
//  Created by Sergey Yefanov on 22.03.16.
//  Copyright © 2016 Sergey Yefanov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCYPointsCreater : NSObject

+ (NSArray *)getPoints;

@end
